<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('name');
	        $table->string('unit');
	        $table->string('price')->default(0);
	        $table->string('amount')->default(0);
	        $table->unsignedInteger('category_id')->insigned()->nullable();
	        $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
