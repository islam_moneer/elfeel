<?php
return [
	'create' => 'انشاء',
	'edit' => 'تعديل',
	'update' => 'تحديث',
	'delete' => 'حذف',
	'view' => 'عرض',
	'back' => 'عودة',
];