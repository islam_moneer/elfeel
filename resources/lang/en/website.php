<?php
return [
	'title' => 'الفيل الدولية للصناعة',
	'login' => 'تسجيل دخول',
	'register' => 'تسجيل جديد',
	'logout' => 'تسجيل الخروج',
	'email'     => 'البريد الإلكتروني',
	'username' => 'أسم المستخدم',
	'password' => 'كلمة المرور',
	'c_password' => 'تأكيد كلمة المرور',
	'remember_me' => 'تذكرني',
	'forget_password' => 'نسيت كلمة المرور',

];