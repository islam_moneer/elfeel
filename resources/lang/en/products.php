<?php

return [
    'name' => 'الاسم',
	'unit' => 'الوحدة',
	'price' => 'السعر',
	'amount' => 'الكمية',
	'products' => 'المنتجات',
	'product' => 'المنتج',
	'id'    => '#',
];
