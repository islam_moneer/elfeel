<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'كلمة المرور لابد ألا تقل عن 6 احرق وتطابق تأكيد كلمة المرور',
    'reset' => 'تم تغيير كلمة المرور!',
    'sent' => 'تم إرسال رابط اعادة تسجيل كلمة مرور جديدة على بريدك الإلكتروني',
    'token' => 'رمز إعادة تعيين كلمة مرور غير صحيح',
    'user' => "لا يوجد مستخدم بهذا البريد الإلكتروني",
    'user' => "We can't find a user with that e-mail address.",

];
