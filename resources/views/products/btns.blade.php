<a href="{{ url("/products/{$item->id}") }}" title="{{ trans('main.view') }} {{ trans('products.the_product') }}"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> {{ trans('main.view') }}</button></a>
<a href="{{ url("/products/{$item->id}/edit") }}" title="{{ trans('main.edit') }} {{ trans('products.the_product') }}"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('main.edit') }}</button></a>
{!! Form::open([
    'method' =>'DELETE',
    'url'    => ["/product", $item->id],
    'style'  => 'display:inline'
]) !!}
{!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '. trans('main.delete'), array(
		'type'    => 'submit',
		'class'   => 'btn btn-danger btn-xs',
		'title'   => trans('main.delete'). ' ' .trans('products.the_product'),
		'onclick' =>'return confirm("'.trans('main.confirm_del').'")'
)) !!}
{!! Form::close() !!}
