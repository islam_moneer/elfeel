@extends('layouts.app')

@section('css-styles')
    <link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('products.products') }}</div>
                    <div class="panel-body">
                        <a  href="{{ url('/products/create') }}"
                            class="btn btn-success btn-sm"
                            title="{{ trans('main.create') }} Product"
                            dusk="create-Product">
                            <i class="fa fa-plus" aria-hidden="true"></i> {{ trans('main.create') }}
                        </a>

                        <div class="clear-fix"></div>

                        <br/>
                        <br/>
                        <div class="table-responsive datatables">
                            <table class="table table-borderless table-striped table-hover" id="datatable">
                                <thead>
                                    <tr>
                                        <th>{{ trans('products.id') }}</th>
                                        <th>{{ trans('products.name') }}</th>
                                        <th>{{ trans('products.unit') }}</th>
                                        <th>{{ trans('products.price') }}</th>
                                        <th>{{ trans('products.amount') }}</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js-scripts')
    <!-- DataTables -->
    <script type="text/javascript" charset="utf8" src="{{ asset('js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" charset="utf8" src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
@include('products.datatables')
    <script>
        {{--$(function() {--}}
            {{--$('#datatable').DataTable({--}}
                {{--processing: true,--}}
                {{--serverSide: true,--}}
                {{--ajax: '{!! route('products.index') !!}',--}}
                {{--columns: [--}}
                    {{--{ data: 'id', name: 'id' },--}}
                    {{--{ data: 'name', name: 'name' },--}}
                    {{--{ data: 'unit', name: 'unit' },--}}
                    {{--{ data: 'price', name: 'price' },--}}
                    {{--{ data: 'amount', name: 'amount' },--}}
                    {{--{ data: 'actions', name: 'actions', orderable: false, searchable: false }--}}
                {{--]--}}
            {{--});--}}
        {{--});--}}
    </script>
@endsection